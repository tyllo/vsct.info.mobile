Framework7-VueJS
===========

Use for auth: demo/demo

This app shows you example of using mobile framework - Framework7 with [VueJS](http://vuejs.org/), [Vuex](http://vuex.vuejs.org/), [vue-router](http://vuejs.github.io/vue-router/en/index.html), [vue-i18n](https://github.com/kazupon/vue-i18n) to build 'personal account' application.


Features
------

webpack - bundler
gulp - task mnager
svg-spites - for icon
css modules - future of css
jade - templater
scss - preprocessor


How to build
------

- for development use `gulp --develope`
- for build minimize use `gulp --production`
- for debug use `gulp --debug`



Thanks
------

Licensed under the [MIT License](https://opensource.org/licenses/MIT)
